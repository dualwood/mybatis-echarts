import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONPObject;
import com.lin.Dao.CompanyMapper;
import com.lin.Dao.PatentMapper;
import com.lin.Dao.ProvinceMapper;
import com.lin.IO.ListToJson;
import com.lin.pojo.Company;
import com.lin.pojo.Patent;
import com.lin.pojo.Province;
import com.lin.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @author ChenweiLin
 * @create 2020-12-14 14:58
 */
public class MyTest {
    @Test
    public void getTeacherByIdtest() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PatentMapper mapper = sqlSession.getMapper(PatentMapper.class);

        List<Patent> patents = mapper.queryHighValuePatent();

        System.out.println(patents);

        sqlSession.close();

        System.out.println("***************");
        JSONPObject HighValuePatentObject = (JSONPObject) JSON.toJSON(patents);
    }

    @Test
    public void getHighValuePatentJsonObject() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PatentMapper mapper = sqlSession.getMapper(PatentMapper.class);

        List<Patent> patents = mapper.queryHighValuePatent();


        sqlSession.close();
        JSONArray array = (JSONArray) JSON.toJSON(patents);
        System.out.println(array);
    }

    @Test
    public void getHighValueCompanytest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        CompanyMapper mapper = sqlSession.getMapper(CompanyMapper.class);

        List<Company> highValueCompany = mapper.getHighValueCompany();

        sqlSession.close();
        JSONArray array = (JSONArray) JSON.toJSON(highValueCompany);
        System.out.println(array);
    }

    @Test
    public void test3(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        ProvinceMapper mapper = sqlSession.getMapper(ProvinceMapper.class);

        List<Province> provinces = mapper.getProvinces();
        for (Province province : provinces) {
            System.out.println(province);
        }
        sqlSession.close();
        JSONArray array = (JSONArray) JSON.toJSON(provinces);
        System.out.println(array);
    }
}
