package com.lin.Dao;

import com.lin.pojo.Province;

import java.util.List;

/**
 * @author ChenweiLin
 * @create 2020-12-15 19:03
 */
public interface ProvinceMapper {
    //地图数据，省份及专利数量
    List<Province> getProvinces();
}
