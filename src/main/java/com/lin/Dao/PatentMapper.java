package com.lin.Dao;

import com.lin.pojo.Patent;

import javax.management.Query;
import java.util.List;

/**
 * @author ChenweiLin
 * @create 2020-12-14 14:54
 */
public interface PatentMapper  {
    //左上角图标，显示高价值专利
    List<Patent> queryHighValuePatent();
}
