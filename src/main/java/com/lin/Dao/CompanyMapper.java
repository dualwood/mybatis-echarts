package com.lin.Dao;

import com.lin.pojo.Company;

import java.util.List;

/**
 * @author ChenweiLin
 * @create 2020-12-14 17:12
 */
public interface CompanyMapper {
    //获取高价值公司
    List<Company> getHighValueCompany();
}
