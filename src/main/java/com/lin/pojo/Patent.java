package com.lin.pojo;

/**
 * @author ChenweiLin
 * @create 2020-12-14 14:49
 */
public class Patent {
    private String title;
    private String applicant_country;
    private double value_score;

    public Patent() {
    }

    public Patent(String title, String applicant_country, double value_score) {
        this.title = title;
        this.applicant_country = applicant_country;
        this.value_score = value_score;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getApplicant_country() {
        return applicant_country;
    }

    public void setApplicant_country(String applicant_country) {
        this.applicant_country = applicant_country;
    }

    public double getValue_score() {
        return value_score;
    }

    public void setValue_score(double value_score) {
        this.value_score = value_score;
    }

    @Override
    public String toString() {
        return "{" +
                "title='" + title + '\'' +
                ", applicant_country='" + applicant_country + '\'' +
                ", value_score=" + value_score +
                '}';
    }
}
