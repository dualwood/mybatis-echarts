package com.lin.pojo;

/**
 * @author ChenweiLin
 * @create 2020-12-15 19:06
 */
public class Province {
    private String province;
    private int four_num;
    private int five_num;

    public Province() {
    }

    public Province(String province, int four_num, int five_num) {
        this.province = province;
        this.four_num = four_num;
        this.five_num = five_num;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getFour_num() {
        return four_num;
    }

    public void setFour_num(int four_num) {
        this.four_num = four_num;
    }

    public int getFive_num() {
        return five_num;
    }

    public void setFive_num(int five_num) {
        this.five_num = five_num;
    }

    @Override
    public String toString() {
        return "{" +
                "province='" + province + '\'' +
                ", four_num=" + four_num +
                ", five_num=" + five_num +
                '}';
    }
}
