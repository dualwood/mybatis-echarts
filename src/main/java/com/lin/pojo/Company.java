package com.lin.pojo;

/**
 * @author ChenweiLin
 * @create 2020-12-14 17:10
 */
public class Company {
    private String company;//公司名称
    private int first_claim;

    public Company() {
    }

    public Company(String company, int first_claim) {
        this.company = company;
        this.first_claim = first_claim;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getFirst_claim() {
        return first_claim;
    }

    public void setFirst_claim(int first_claim) {
        this.first_claim = first_claim;
    }

    @Override
    public String toString() {
        return "{" +
                "company='" + company + '\'' +
                ", first_claim=" + first_claim +
                '}';
    }
}
