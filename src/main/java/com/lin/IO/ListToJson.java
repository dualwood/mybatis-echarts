package com.lin.IO;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.lin.Dao.CompanyMapper;
import com.lin.Dao.PatentMapper;
import com.lin.Dao.ProvinceMapper;
import com.lin.pojo.Company;
import com.lin.pojo.Patent;
import com.lin.pojo.Province;
import com.lin.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @author ChenweiLin
 * @create 2020-12-14 15:20
 */
public class ListToJson extends com.alibaba.fastjson.JSONObject {

    public JSONArray getHighValuePatentJsonObject() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PatentMapper mapper = sqlSession.getMapper(PatentMapper.class);

        List<Patent> patents = mapper.queryHighValuePatent();

        sqlSession.close();

        JSONArray array = (JSONArray) JSON.toJSON(patents);

        return array;
    }

    public JSONArray getHighValueCompanyJsonObject() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        CompanyMapper mapper = sqlSession.getMapper(CompanyMapper.class);

        List<Company> companies = mapper.getHighValueCompany();


        sqlSession.close();

        JSONArray array = (JSONArray) JSON.toJSON(companies);

        return array;
    }

    public JSONArray getProvinces(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        ProvinceMapper mapper = sqlSession.getMapper(ProvinceMapper.class);

        List<Province> provinces = mapper.getProvinces();

        sqlSession.close();
        JSONArray array = (JSONArray) JSON.toJSON(provinces);
        return array;
    }
}
