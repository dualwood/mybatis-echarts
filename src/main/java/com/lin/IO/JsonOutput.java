package com.lin.IO;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONPObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author ChenweiLin
 * @create 2020-12-02 20:52
 */

@WebServlet(urlPatterns = "/HighPatent")
public class JsonOutput extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        JSONArray highValuePatentJsonObject = new ListToJson().getHighValuePatentJsonObject();
        resp.getWriter().write(highValuePatentJsonObject.toString());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            doGet(req,resp);
    }
}
