package com.lin.IO;

import com.alibaba.fastjson.JSONArray;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.midi.Soundbank;
import java.io.IOException;

/**
 * @author ChenweiLin
 * @create 2020-12-14 17:57
 */
@WebServlet("/CompanyOutput")
public class CompanyOutput extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            resp.setCharacterEncoding("utf-8");
            resp.setContentType("text/html;charset=utf-8");
            JSONArray highValueCompanyJsonObject = new ListToJson().getHighValueCompanyJsonObject();
            resp.getWriter().write(highValueCompanyJsonObject.toString());
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            doGet(req,resp);
        }

}
