# 专利数据可视化大屏展示

#### 系统简介
专利局大屏展示 后端mybatis 前端ECharts


#### 编译与发布

```
# 修改数据库链接信息
vi src/main/resources/db.properties

# 编译代码
mvn package -Dmaven.test.skip=true

# 系统发布
cp target/mybatis-echart.war  $TOMCAT_HOME/webapps/patent.war

```
